-- предполаагем что БД создана
-- CREATE DATABASE retail;

\c retail

CREATE SCHEMA retail;

SET search_path TO retail;

\ir schema.sql
\ir data.sql
\ir procedures.sql