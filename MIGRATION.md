# Миграция БД Postgres через подписки

Предположим, что необходимо мигрировать данные с одного хоста БД в другую
каким-то образом необходимо произвести миграцию данных и схемы данных

Этап миграции подразумевает два основных этапа:
1. Миграция схемы БД
2. Миграция данных в БД

Для иммитации будем использовать БД, запущенную в docker-container на сервере



# Миграция данных в облако репликацией (Postgres)

### Prerequities:
* postges 15
* docker
* postresclient15 (pg_dump, pg_restore, psql)

> При миграции в YC Managed Postgres не использовать user `postgres`, данный пользователь зарезервирован платформой

### Настройки среды
```shell
PUBLISHER_PASSWORD
PUBLISHER_DB
PUBLISHER_USER
PUBLISHER_PUBLIC_PORT
PUBLISHER_HOST


SUBSCRIBER_PASSWORD
SUBSCRIBER_DB
SUBSCRIBER_USER
SUBSCRIBER_PUBLIC_PORT
SUBSCRIBER_HOST
```


## Запуск Publisher DB
```shell
docker run --name publisher-db \
      -e POSTGRES_PASSWORD=${PUBLISHER_PASSWORD} \
      -e POSTGRES_DB=${PUBLISHER_DB} \
      -e POSTGRES_USER=${PUBLISHER_USER} \
      -p PUBLISHER_PUBLIC_PORT:5432 -d \
      -v postgres_settings:/var/lib/postgresql/data \
      postgres:15
```
#### Строки проверки подключения к БД
```shell
psql "host=${PUBLISHER_HOST} port=${PUBLISHER_PUBLIC_PORT} dbname=${PUBLISHER_DB} user=${PUBLISHER_USER} password=${PUBLISHER_PASSWORD}"
```
или
```shell
psql -h ${PUBLISHER_HOST} \
     -p ${PUBLISHER_PUBLIC_PORT} \
     -U ${PUBLISHER_USER} \
     -W ${PUBLISHER_DB}
```
### Выставляем уровень логирования для WAL, для добавления информации для логической репликации (restart is needed)
В файле `postgresql.conf`
```shell
wal_level=logical
```
### Разрешить аунтефикацию в СУБД с хостов Subscriser-кластера
В файлике `pg_hba.conf`
Без SSL:
вместо md5 я использовал password
```txt
host    all            all             <адрес хоста-подписчика>      md5
host    replication    all             <адрес хоста-подписчика>      md5
```
C SSL:
```txt
hostssl    all            all             <адрес хоста-подписчика>      md5
hostssl    replication    all             <адрес хоста-подписчика>      md5      
```

## Накат тестовых скриптов и данных
```shell
psql "host=${PUBLISHER_HOST} port=${PUBLISHER_PUBLIC_PORT} dbname=${PUBLISHER_DB} user=${PUBLISHER_USER} password=${PUBLISHER_PASSWORD}" \
-f <path_to_intstall_script.sql>
```

## Создать PUBLISHER
```sql
CREATE PUBLICATION <publication_name> FOR TABLES IN SCHEMA <schema>;
```
или для всех таблиц
```sql
CREATE PUBLICATION <publication_name> FOR ALL TABLES;
```
и проверка существующих публикаций
```sql
SELECT * FROM pg_publication;
```
и подписок
```sql
SELECT * FROM pg_subscription;
```

## Создать dump схемы данных publisher
```shell
pg_dump -h ${PUBLISHER_HOST} \
    -U ${PUBLISHER_USER} \
    -p ${PUBLISHER_PUBLIC_PORT} \
    --schema-only \
    --no-privileges \
    --no-subscriptions \
    -d ${PUBLISHER_DB} \
     -Fd -f <path_to_store_dump>
```
при экспорте исключаются все данные, которые связаны с привилегиями и ролями, чтобы не возникало конфликтов с настройками БД в YC

## Запуск Cluster Consumer DB (Managed Postgres 15)
выдать права `mdb_replication` пользователю, под которым будем проводить миграцию или `GRANT REPLICATION TO <user>;`

## Восстановить dump на БД-subscriber
```shell
pg_restore -Fd -v \
         --single-transaction \
         -s --no-privileges \
          -h ${SUBSCRIBER_HOST} \
          -U ${SUBSCRIBER_USER} \
          -p ${SUBSCRIBER_PUBLIC_PORT} \
          -d ${SUBSCRIBER_DB} <path_to_get_dump>
```

## Создать подписку на subscriber db
Основной синтаксис создания
```sql
CREATE SUBSCRIPTION <subscribtion_name> CONNECTION <connection string> PUBLICATION <publication_name>;
```
Создание подписки без SSL
```sql
CREATE SUBSCRIPTION <subscribtion_name> CONNECTION 'host=${PUBLISHER_HOST} port=${PUBLISHER_PUBLIC_PORT} user=${PUBLISHER_USER} sslmode=disable dbname=${PUBLISHER_DB} password=${PUBLISHER_PASSWORD}' PUBLICATION <publication_name>;
```

Создание подписки c SSL
```sql
CREATE SUBSCRIPTION <subscribtion_name> CONNECTION 'host=${PUBLISHER_HOST} port=${PUBLISHER_PUBLIC_PORT} user=${PUBLISHER_USER} sslmode=verify-full dbname=${PUBLISHER_DB} password=${PUBLISHER_PASSWORD}' PUBLICATION <publication_name>;
```

> Отличие в значение параметра `sslmode`


## Мониторинг репликации
`pg_stat_subscription` - каталоги статуса репликации

```sql
select * from pg_subscription_rel;
```

```sql
select * from pg_stat_replication;
```


## Перенос sequesess (мы не делали)
```shell
pg_dump -h <адрес сервера СУБД> \
    -U <имя пользователя> \
    -p <порт> -d <имя базы данных> \
    --data-only -t '*.*_seq' > /tmp/seq-data.sql
```
 
```shell
psql -h <адрес сервера СУБД> \
    -U <имя пользователя> -p 6432 \
    -d <имя базы данных> \
    < /tmp/seq
```


## Удаление подписки и публикации
```sql
DROP SUBSCRIPTION <subscribtion_name>;
DROP PUBLICATION <publication_name>;
```

### Активные транзакции
```sql
SELECT * FROM pg_stat_activity WHERE state = 'active';
```