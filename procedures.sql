CREATE OR REPLACE PROCEDURE retail.generate_customers()
LANGUAGE plpgsql
AS $$
DECLARE
  i INT := 1;
  first_name VARCHAR(50);
  last_name VARCHAR(50);
  email VARCHAR(255);
  phone VARCHAR(20);
BEGIN
  WHILE i <= 1000 LOOP
    first_name := substr(md5(random()::text), 1, 10);
    last_name := substr(md5(random()::text), 1, 10);
    email := first_name || '.' || last_name || '@example.com';
    phone := substr(md5(random()::text), 1, 10);
    INSERT INTO retail.customers (first_name, last_name, email, phone) 
    VALUES (first_name, last_name, email, phone);
    i := i + 1;
  END LOOP;
END;
$$;
