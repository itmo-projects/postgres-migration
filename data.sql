-- Insert customers
INSERT INTO retail.customers (first_name, last_name, email, phone) VALUES
('John', 'Doe', 'john.doe@example.com', '123-456-7890'),
('Jane', 'Doe', 'jane.doe@example.com', '987-654-3210'),
('Bob', 'Smith', 'bob.smith@example.com', '555-555-5555');

-- Insert orders
INSERT INTO retail.orders (customer_id, order_date, total_amount) VALUES
(1, '2022-03-28', 150.00),
(1, '2022-03-30', 75.00),
(2, '2022-03-29', 200.00),
(3, '2022-03-31', 50.00);

-- Insert products
INSERT INTO retail.products (name, price, description) VALUES
('Product 1', 10.99, 'Description of Product 1'),
('Product 2', 25.50, 'Description of Product 2'),
('Product 3', 7.99, 'Description of Product 3');

-- Insert categories
INSERT INTO retail.categories (name, description) VALUES
('Category 1', 'Description of Category 1'),
('Category 2', 'Description of Category 2');

-- Insert orders_products
INSERT INTO retail.orders_products (order_id, product_id, quantity, price) VALUES
(1, 1, 2, 10.99),
(1, 2, 1, 25.50),
(2, 3, 3, 7.99),
(3, 1, 1, 10.99),
(3, 2, 2, 25.50),
(3, 3, 1, 7.99),
(4, 2, 1, 25.50);

-- Insert customers_addresses
INSERT INTO retail.customers_addresses (customer_id, address_line_1, city, state, country, zip_code) VALUES
(1, '123 Main St', 'Anytown', 'CA', 'USA', '12345'),
(2, '456 Elm St', 'Othertown', 'CA', 'USA', '67890');

-- Insert suppliers
INSERT INTO retail.suppliers (name, email, phone) VALUES
('Supplier 1', 'supplier1@example.com', '111-222-3333'),
('Supplier 2', 'supplier2@example.com', '444-555-6666');

-- Insert products_suppliers
INSERT INTO retail.products_suppliers (product_id, supplier_id) VALUES
(1, 1),
(2, 2),
(3, 1),
(3, 2);

-- Insert reviews
INSERT INTO retail.reviews (product_id, customer_id, rating, comment) VALUES
(1, 1, 4, 'Good product'),
(1, 2, 5, 'Great product'),
(2, 2, 3, 'Average product'),
(3, 3, 2, 'Bad product');