-- Информация о всех товарах, заказанных определенным клиентом, вместе с информацией о заказе и информацией о клиенте.
SELECT p.product_id, p.name, p.price, op.quantity, op.price as total_price, o.order_date, c.first_name, c.last_name, c.email
FROM retail.orders o
JOIN retail.customers c ON o.customer_id = c.customer_id
JOIN retail.orders_products op ON o.order_id = op.order_id
JOIN retail.products p ON op.product_id = p.product_id
WHERE c.email = 'john.doe@example.com';

-- Список всех отзывов, оставленных конкретным покупателем, включая название продукта
SELECT r.rating, r.comment, r.created_at, p.name as product_name
FROM retail.reviews r
JOIN retail.products p ON r.product_id = p.product_id
JOIN retail.customers cust ON r.customer_id = cust.customer_id
WHERE cust.email = 'john.doe@example.com';

-- Информацию обо всех заказах на определенный продукт, включая информацию о клиенте и общую сумму каждого заказа
SELECT o.order_id, o.order_date, c.first_name, c.last_name, c.email, SUM(op.price * op.quantity) as total_amount
FROM retail.orders o
JOIN retail.customers c ON o.customer_id = c.customer_id
JOIN retail.orders_products op ON o.order_id = op.order_id
JOIN retail.products p ON op.product_id = p.product_id
WHERE p.name = 'Product 1'
GROUP BY o.order_id, o.order_date, c.first_name, c.last_name, c.email;

-- топ клиентов, которые потратили больше всего денег на заказы
SELECT c.customer_id, c.first_name, c.last_name, c.email, SUM(o.total_amount) as total_spent
FROM retail.customers c
JOIN retail.orders o ON c.customer_id = o.customer_id
GROUP BY c.customer_id
ORDER BY total_spent DESC;